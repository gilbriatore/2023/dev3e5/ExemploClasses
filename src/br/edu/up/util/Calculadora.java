package br.edu.up.util;

public class Calculadora {

	public double somar(double a, double b) {
		return a + b;
	}
	
	public double subrair(double a, double b) {
		return a - b;
	}
	
	public double multiplicar(double a, double b) {
		return a * b;
	}
	
	public double dividir(double a, double b) {
		return a / b;
	}
	
}
