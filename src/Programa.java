//import br.edu.up.util.Console;
//import br.edu.up.util.Calculadora;
import br.edu.up.util.*;

public class Programa extends Console {

	
	public static void main(String[] args) {
		
		
		double valor1 = 0;
		double valor2 = 0;
		double resultado = 0;
		
		do {						
			valor1 = lerDecimal("Informe o primeiro valor:"); 						
			valor2 = lerDecimal("Informe o segundo valor:");

			br.edu.up.util.Console.separador();

			Calculadora oObjeto = new Calculadora();					
			//resultado = valor1 + valor2;
			resultado = oObjeto.somar(valor1, valor2);
						
			separador();
			imprimir("O resultado é: " + resultado);
			separador();
			
		
		} while (resultado > 0);
		
		separador();
		imprimir("Programa encerrado!");		
		
	}
	
}
